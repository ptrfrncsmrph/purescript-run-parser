module Run.Parser
  ( AnySandboxedFile
  , Error
  , Expected
  , Miss
  , PARSER
  , Parser
      ( Parser
      )
  , Position
  , Result
  , State
  , Unexpected
  , _parser
  , chunk
  , endOfInput
  , get
  , handleExpected
  , handleResult
  , handleUnexpected
  , lift
  , map
  , modify
  , prettyError
  , run
  , throw
  ) where

import Prelude

import Control.Monad.Rec.Class
  ( Step
  )
import Data.Either
  ( Either
  , either
  )
import Data.Functor.Variant
  ( FProxy
  , VariantF
  )
import Data.List
  ( List(..)
  , (:)
  )
import Data.Natural
  ( Natural
  )
import Data.Set
  ( Set
  )
import Data.String.NonEmpty
  ( NonEmptyString
  )
import Data.Symbol
  ( class IsSymbol
  , SProxy(..)
  )
import Data.Tuple
  ( Tuple(..)
  )
import Data.Variant
  ( Variant
  )
import Pathy
  ( Abs
  , File
  , Rel
  , SandboxedPath
  )
import Prim.Row
  ( class Cons
  , class Union
  )
import Run
  ( Run
  )
import Run.Except
  ( Except
  )
import Type.Row
  ( type (+)
  )

import Control.Monad.Rec.Class as Control.Monad.Rec.Class
import Data.Functor.Variant as Data.Functor.Variant
import Data.Set as Data.Set
import Data.String.NonEmpty as Data.String.NonEmpty
import Data.Variant as Data.Variant
import Pathy as Pathy
import Run as Run
import Run.Except as Run.Except
import Unsafe.Coerce as Unsafe.Coerce

type AnySandboxedFile
  = Either (SandboxedPath Abs File) (SandboxedPath Rel File)

type Error
  = {position :: Position, unexpected :: Unexpected, expected :: Set Expected}

type Expected
  = Variant (chunk :: NonEmptyString, endOfInput :: Unit)

type HandleExpected a
  = {chunk :: NonEmptyString -> a, endOfInput :: Unit -> a}

type HandleResult a b
  = {error :: Error -> b, result :: a -> b}

type HandleUnexpected a
  = {chunk :: NonEmptyString -> a, endOfInput :: Unit -> a, nothing :: Unit -> a}

type Miss r
  = {expected :: Set Expected, unexpected :: Unexpected | r}

type Position
  = {column :: Natural, line :: Natural, name :: AnySandboxedFile}

type Result a
  = Variant (error :: Error, result :: a)

type State
  = {position :: Position, rest :: String}

type Unexpected
  = Variant (chunk :: NonEmptyString, endOfInput :: Unit, nothing :: Unit)

type EXCEPT e r
  = (except :: FProxy (Except e) | r)

type PARSER r
  = (parser :: FProxy Parser | r)

newtype Parser a
  = Parser (State -> {result :: Result a, state :: State})

instance functorParser ::
  Functor Parser where
    map f (Parser g) = Parser \s ->
      case g s of
        x -> { result: go _result f x.result
             , state: x.state
             }
      where
      go ::
        forall a b r1 r2 s t.
        Cons s a t r1 =>
        Cons s b t r2 =>
        IsSymbol s =>
        SProxy s ->
        (a -> b) ->
        Variant r1 ->
        Variant r2
      go s h = Data.Variant.on s (Data.Variant.inj s <<< h) Unsafe.Coerce.unsafeCoerce

lift ::
  forall a r.
  (State -> {result :: Result a, state :: State}) ->
  Run (PARSER + r) a
lift f = Run.lift _parser (Parser f)

map ::
  forall a r.
  (Parser ~> Parser) ->
  Run (PARSER + r) a ->
  Run (PARSER + r) a
map f = Run.interpret go
  where
  go ::
    VariantF (PARSER + r)
      ~> Run (PARSER + r)
  go = Run.on _parser lift' send
  lift' ::
    Parser
      ~> Run (PARSER + r)
  lift' x = Run.lift _parser (f x)
  send ::
    VariantF r
      ~> Run (PARSER + r)
  send x = Unsafe.Coerce.unsafeCoerce (Run.send x)

_chunk ::
  SProxy "chunk"
_chunk = SProxy

_endOfInput ::
  SProxy "endOfInput"
_endOfInput = SProxy

_error ::
  SProxy "error"
_error = SProxy

_parser ::
  SProxy "parser"
_parser = SProxy

_result ::
  SProxy "result"
_result = SProxy

-- Error helpers
endOfInput ::
  forall r.
  Variant (endOfInput :: Unit | r)
endOfInput = Data.Variant.inj _endOfInput unit

chunk ::
  forall r.
  NonEmptyString ->
  Variant (chunk :: NonEmptyString | r)
chunk = Data.Variant.inj _chunk

prettyError ::
  Error ->
  String
prettyError = case _ of
  { expected, position, unexpected } -> either (Pathy.printPath Pathy.posixPrinter) (Pathy.printPath Pathy.posixPrinter) position.name <> ":" <> show position.line <> ":" <> show position.column <> ": Parsing failure." <> prettyExpecteds expected <> handleUnexpected prettyUnexpected unexpected

prettyExpected ::
  HandleExpected String
prettyExpected = { chunk: \x ->
                   show (Data.String.NonEmpty.toString x)
                 , endOfInput: \_ ->
                   "`EOF`"
                 }

-- Print the collection with an Oxford comma.
prettyExpecteds ::
  Set Expected ->
  String
prettyExpecteds xs' = case Data.Set.toUnfoldable xs' of
  Nil -> ""
  x : Nil -> " Expected one of the following: " <> handleExpected prettyExpected x <> ". "
  x : y : Nil -> " Expected one of the following: " <> handleExpected prettyExpected x <> " or " <> handleExpected prettyExpected y <> ". "
  xs -> " Expected one of the following: " <> prettyOxford "" xs <> ". "

prettyOxford ::
  String ->
  List Expected ->
  String
prettyOxford acc = case _ of
  Nil -> ""
  x : Nil -> acc <> "or " <> handleExpected prettyExpected x
  x : y -> prettyOxford (acc <> handleExpected prettyExpected x <> ", ") y

prettyUnexpected ::
  HandleUnexpected String
prettyUnexpected = { chunk: \x ->
                     "Did not expect to see: " <> show (Data.String.NonEmpty.toString x) <> "."
                   , endOfInput: \_ ->
                     "Ran out of input."
                   , nothing: \_ ->
                     ""
                   }

throw ::
  forall a r s.
  Miss s ->
  Run (PARSER + r) a
throw { expected, unexpected } = do
  state@{ position } <- get
  let error = { expected
              , position
              , unexpected
              }
  lift \_ ->
    { result: Data.Variant.inj _error error
    , state
    }

-- Handlers
handleExpected ::
  forall a.
  HandleExpected a ->
  Expected ->
  a
handleExpected = Data.Variant.match

handleResult ::
  forall a b.
  HandleResult a b ->
  Result a ->
  b
handleResult = Data.Variant.match

handleUnexpected ::
  forall a.
  HandleUnexpected a ->
  Unexpected ->
  a
handleUnexpected = Data.Variant.match

-- Runners
run ::
  forall a r.
  Union r (EXCEPT Error + ()) (EXCEPT Error + r) =>
  AnySandboxedFile ->
  String ->
  Run (PARSER + r) a ->
  Run (EXCEPT Error + r) a
run name rest = Run.runAccumPure go (flip const) state
  where
  expandler ::
    VariantF r (Run (PARSER + r) a) ->
    Step (Tuple State (Run (PARSER + r) a)) (VariantF (EXCEPT Error + r) (Run (PARSER + r) a))
  expandler = Control.Monad.Rec.Class.Done <<< Data.Functor.Variant.expand
  go ::
    State ->
    VariantF (PARSER + r) (Run (PARSER + r) a) ->
    Step (Tuple State (Run (PARSER + r) a)) (VariantF (EXCEPT Error + r) (Run (PARSER + r) a))
  go x = Data.Functor.Variant.on _parser (handler x) expandler
  handler ::
    State ->
    Parser (Run (PARSER + r) a) ->
    Step (Tuple State (Run (PARSER + r) a)) (VariantF (EXCEPT Error + r) (Run (PARSER + r) a))
  handler x' (Parser f) = case f x' of
    x -> handleResult { error: onError
                      , result: onResult x.state
                      } x.result
  onError ::
    Error ->
    Step (Tuple State (Run (PARSER + r) a)) (VariantF (EXCEPT Error + r) (Run (PARSER + r) a))
  onError x = Control.Monad.Rec.Class.Done (Data.Functor.Variant.inj Run.Except._except $ Run.Except.Except x)
  onResult ::
    State ->
    Run (PARSER + r) a ->
    Step (Tuple State (Run (PARSER + r) a)) (VariantF (EXCEPT Error + r) (Run (PARSER + r) a))
  onResult x y = Control.Monad.Rec.Class.Loop (Tuple x y)
  position ::
    Position
  position = { column: zero
             , line: zero
             , name
             }
  state ::
    State
  state = { position
          , rest
          }

-- State
modify ::
  forall r.
  (State -> State) ->
  Run (PARSER + r) Unit
modify f = lift \state ->
  { result: Data.Variant.inj _result unit
  , state: f state
  }

get ::
  forall r.
  Run (PARSER + r) State
get = lift \state ->
  { result: Data.Variant.inj _result state
  , state
  }
