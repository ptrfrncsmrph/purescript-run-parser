module Run.Parser.Combinator
  ( (<?>)
  , chunk
  , codePoint
  , eof
  , label
  , labelFlipped
  , string
  , token
  , try
  ) where

import Prelude

import Data.Either
  ( Either(..)
  , either
  )
import Data.Foldable
  ( foldMap
  )
import Data.Maybe
  ( Maybe(..)
  )
import Data.String
  ( CodePoint
  )
import Data.String.NonEmpty
  ( NonEmptyString
  )
import Data.Symbol
  ( SProxy(..)
  )
import Run
  ( Run
  )
import Run.Parser
  ( Error
  , Expected
  , Miss
  , PARSER
  , Parser(..)
  , Result
  )
import Type.Row
  ( type (+)
  )

import Data.Array as Data.Array
import Data.Set as Data.Set
import Data.String as Data.String
import Data.String.NonEmpty as Data.String.NonEmpty
import Data.Variant as Data.Variant
import Run.Parser as Run.Parser

advance ::
  forall column line r.
  Semiring column =>
  Semiring line =>
  NonEmptyString ->
  {column :: column, line :: line | r} ->
  {column :: column, line :: line | r}
advance x position = Data.Array.foldl (flip increment) position (Data.String.NonEmpty.toCodePointArray x)

codePoint ::
  forall r.
  CodePoint ->
  Run (PARSER + r) CodePoint
codePoint code = token go (pure code)
  where
  go c
    | code == c = pure c
    | otherwise = Left { expected: Data.Set.singleton (Run.Parser.chunk $ Data.String.NonEmpty.singleton code)
                       , unexpected: Run.Parser.chunk (Data.String.NonEmpty.singleton c)
                       }

chunk ::
  forall a r s.
  (NonEmptyString -> Either (Miss s) a) ->
  NonEmptyString ->
  Run (PARSER + r) a
chunk f x' = do
  { rest } <- Run.Parser.get
  case splitAt (Data.String.NonEmpty.length x') rest of
    { before: Nothing } -> Run.Parser.throw { expected: Data.Set.singleton (Run.Parser.chunk x')
                                            , unexpected: Run.Parser.endOfInput
                                            }
    { after, before: Just x } -> either Run.Parser.throw (hit after) (f x)
  where
  hit rest x = do
    Run.Parser.modify \{ position } ->
      { position: advance x' position
      , rest
      }
    pure x

eof ::
  forall r.
  Run (PARSER + r) Unit
eof = do
  { rest } <- Run.Parser.get
  case Data.String.uncons rest of
    Nothing -> pure unit
    Just { head, tail } -> Run.Parser.throw { expected: Data.Set.singleton Run.Parser.endOfInput
                                            , unexpected: Run.Parser.chunk (Data.String.NonEmpty.cons head tail)
                                            }

increment ::
  forall column line r.
  Semiring column =>
  Semiring line =>
  CodePoint ->
  {column :: column, line :: line | r} ->
  {column :: column, line :: line | r}
increment x position = position { column = column
                                , line = line
                                }
  where
  code = Data.String.codePointFromChar
  column
    | code '\n' == x = zero
    | otherwise = position.column + one
  line
    | code '\n' == x = position.line + one
    | otherwise = position.line

label ::
  forall a r.
  NonEmptyString ->
  Run (PARSER + r) a ->
  Run (PARSER + r) a
label l = Run.Parser.map go
  where
  _error ::
    SProxy "error"
  _error = SProxy
  _result ::
    SProxy "result"
  _result = SProxy
  go ::
    Parser
      ~> Parser
  go (Parser f) = Parser \state ->
    case f state of
      x -> x { result = relabelResult x.result
             }
  relabelError ::
    Error ->
    Error
  relabelError x = x { expected = Data.Set.map relabelExpected x.expected
                     }
  relabelExpected ::
    Expected ->
    Expected
  relabelExpected = Run.Parser.handleExpected { endOfInput: const Run.Parser.endOfInput
                                              , chunk: const (Run.Parser.chunk l)
                                              }
  relabelResult ::
    Result
      ~> Result
  relabelResult = Run.Parser.handleResult { error: Data.Variant.inj _error <<< relabelError
                                          , result: Data.Variant.inj _result
                                          }

infix 0 labelFlipped as <?>

labelFlipped ::
  forall a r.
  Run (PARSER + r) a ->
  NonEmptyString ->
  Run (PARSER + r) a
labelFlipped x l = label l x

splitAt ::
  Int ->
  String ->
  {after :: String, before :: Maybe NonEmptyString}
splitAt n x = case Data.String.splitAt n x of
  { after, before } -> { after
                       , before: Data.String.NonEmpty.fromString before
                       }

string ::
  forall r.
  NonEmptyString ->
  Run (PARSER + r) NonEmptyString
string x = chunk go x
  where
  go str
    | str == x = pure x
    | otherwise = Left { expected: Data.Set.singleton (Run.Parser.chunk x)
                       , unexpected: Run.Parser.chunk str
                       }

token ::
  forall a r s.
  (CodePoint -> Either (Miss s) a) ->
  Maybe CodePoint ->
  Run (PARSER + r) a
token f x = do
  { position, rest } <- Run.Parser.get
  case Data.String.uncons rest of
    Nothing -> Run.Parser.throw { expected
                                , unexpected: Run.Parser.endOfInput
                                }
    Just codes -> either Run.Parser.throw (hit codes) (f codes.head)
  where
  expected = foldMap (Data.Set.singleton <<< Run.Parser.chunk <<< Data.String.NonEmpty.singleton) x
  hit { head, tail } result = do
    Run.Parser.modify \{ position } ->
      { position: increment head position
      , rest: tail
      }
    pure result

try ::
  forall a r.
  Run (PARSER + r) a ->
  Run (PARSER + r) a
try = Run.Parser.map go
  where
  go ::
    Parser
      ~> Parser
  go (Parser f) = Parser \state ->
    case f state of
      x@{ result } -> Run.Parser.handleResult { error: const x { state = state
                                                               }
                                              , result: const x
                                              } result
