# purescript-run-parser

## Motivation

There are quite a few parsing packages in PureScript already, why another one?
Some are transformer based, some have less than ideal error messages,
and almost all of the options are weaker typed than they should be.
They contain combinators like `many1 :: Parser a -> Parser (List a)`,
but they could contain combinators like `many1 :: Parser a -> Parser (NonEmptyList a)`

With this we're going down the [run][] rabbit-hole.
We're also going to make nice error messages an explicit goal.
It's the 21st century, there's no reason we can't have nice error messages.

## Current state

Currently, this package only works on `String`s with `CodePoint` as the underlying token type.
We ought to be able to parameterize that without losing much.

This package is also very barebones.
More will be added shortly.

[run]: https://github.com/natefaubion/purescript-run
