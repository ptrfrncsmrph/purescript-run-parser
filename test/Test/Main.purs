module Test.Main where

import Prelude

import Control.Monad.Gen
  ( class MonadGen
  )
import Control.Monad.Rec.Class
  ( class MonadRec
  )
import Data.Either
  ( Either(..)
  )
import Data.Foldable
  ( for_
  )
import Data.Maybe
  ( Maybe(..)
  )
import Data.String
  ( CodePoint
  )
import Data.String.NonEmpty
  ( NonEmptyString
  )
import Data.Symbol
  ( SProxy(..)
  )
import Effect
  ( Effect
  )
import Effect.Aff
  ( Aff
  )
import Run
  ( Run
  )
import Run.Parser
  ( AnySandboxedFile
  , Error
  , PARSER
  , prettyError
  )
import Test.QuickCheck.Gen
  ( Gen
  )
import Test.Spec
  ( Spec
  )
import Test.Spec.Runner
  ( Reporter
  )
import Type.Row
  ( type (+)
  )

import Data.Char.Gen as Data.Char.Gen
import Data.Natural as Data.Natural
import Data.Set as Data.Set
import Data.String as Data.String
import Data.String.Gen as Data.String.Gen
import Data.String.NonEmpty as Data.String.NonEmpty
import Effect.Class as Effect.Class
import Pathy as Pathy
import Run as Run
import Run.Except as Run.Except
import Run.Parser as Run.Parser
import Run.Parser.Combinator as Run.Parser.Combinator
import Test.QuickCheck.Gen as Test.QuickCheck.Gen
import Test.Spec as Test.Spec
import Test.Spec.Assertions as Test.Spec.Assertions
import Test.Spec.Console as Test.Spec.Console
import Test.Spec.Reporter as Test.Spec.Reporter
import Test.Spec.Runner as Test.Spec.Runner

main ::
  Effect Unit
main = Test.Spec.Runner.run reporters do
  Test.Spec.describe "Run.Parser.Combinator" do
    chunk
    codePoint
    eof
    label
    string
    token
    try

reporters ::
  Array Reporter
reporters = [ Test.Spec.Reporter.consoleReporter
            ]

-- Specs
chunk ::
  Spec Unit
chunk = Test.Spec.describe "chunk" do
  let hello = Data.String.NonEmpty.nes (SProxy :: SProxy "hello")
  Test.Spec.it "fails for empty input" do
    x' <- run' "" (Run.Parser.Combinator.chunk (const $ pure unit) hello)
    case x' of
      Left x -> x.unexpected `Test.Spec.Assertions.shouldEqual` Run.Parser.endOfInput
      Right x -> Test.Spec.Assertions.fail ("Should not have succeeded, but we received: " <> show x)
  Test.Spec.it "fails for the wrong input" do
    let go x
          | hello == x = pure hello
          | otherwise = Left { expected: Data.Set.singleton (Run.Parser.chunk hello)
                             , unexpected: Run.Parser.chunk x
                             }
    x' <- run' "hi" (Run.Parser.Combinator.chunk go hello)
    case x' of
      Left x -> x.unexpected `Test.Spec.Assertions.shouldEqual` Run.Parser.chunk (Data.String.NonEmpty.nes $ SProxy :: SProxy "hi")
      Right x -> Test.Spec.Assertions.fail ("Should not have succeeded, but we received: " <> show x)
  Test.Spec.it "returns the value for a 'pure' parser" $ run " " do
    x <- Run.Parser.Combinator.chunk (const $ pure unit) hello
    x `shouldEqual` unit
  Test.Spec.it "consumes input appropriately" $ run "hello" do
    let string' str x
          | str == x = pure str
          | otherwise = Left { expected: Data.Set.singleton (Run.Parser.chunk str)
                             , unexpected: Run.Parser.chunk x
                             }
    h <- Run.Parser.Combinator.chunk (string' hello) hello
    h `shouldEqual` hello
    state <- Run.Parser.get
    Run.liftEffect (Test.Spec.Console.write "checking column")
    state.position.column `shouldEqual` Data.Natural.intToNat 5
    Run.liftEffect (Test.Spec.Console.write "checking line")
    state.position.line `shouldEqual` zero
    state.rest `shouldEqual` ""

codePoint ::
  Spec Unit
codePoint = Test.Spec.describe "codePoint" do
  Test.Spec.it "succeeds for the corresponding code point" $ quickCheck codePointGen \code ->
    run (Data.String.singleton code) do
      c <- Run.Parser.Combinator.codePoint code
      c `shouldEqual` code
  Test.Spec.it "fails for a wrong code point" do
    x' <- run' " " (Run.Parser.Combinator.codePoint $ point 'a')
    case x' of
      Left x -> do
        x.expected `Test.Spec.Assertions.shouldContain` Run.Parser.chunk (Data.String.NonEmpty.singleton $ point 'a')
        x.unexpected `Test.Spec.Assertions.shouldEqual` Run.Parser.chunk (Data.String.NonEmpty.singleton $ point ' ')
      Right x -> Test.Spec.Assertions.fail ("Should not have succeeded, but we received: " <> show x)

eof ::
  Spec Unit
eof = Test.Spec.describe "eof" do
  Test.Spec.it "succeeded for empty input" $ run "" do
    Run.Parser.Combinator.eof
  Test.Spec.it "fails for non-empty input" do
    x' <- run' " " Run.Parser.Combinator.eof
    case x' of
      Left x -> x.expected `Test.Spec.Assertions.shouldContain` Run.Parser.endOfInput
      Right x -> Test.Spec.Assertions.fail ("Should not have succeeded, but we received: " <> show x)

label ::
  Spec Unit
label = Test.Spec.describe "label" do
  Test.Spec.it "changes the expected message" do
    let hello = Data.String.NonEmpty.nes $ SProxy :: SProxy "hello"
        hello' = Data.String.NonEmpty.nes $ SProxy :: SProxy "The word hello"
    x' <- run' " " (Run.Parser.Combinator.label hello' $ Run.Parser.Combinator.string hello)
    case x' of
      Left x -> do
        x.expected `Test.Spec.Assertions.shouldContain` Run.Parser.chunk hello'
        x.unexpected `Test.Spec.Assertions.shouldEqual` Run.Parser.chunk (Data.String.NonEmpty.singleton $ point ' ')
      Right x -> Test.Spec.Assertions.fail ("Should not have succeeded, but we received: " <> show x)

string ::
  Spec Unit
string = Test.Spec.describe "string" do
  Test.Spec.it "succeeds for the corresponding string" $ quickCheck genUnicodeNonEmptyString \str ->
    run (Data.String.NonEmpty.toString str) do
      s <- Run.Parser.Combinator.string str
      s `shouldEqual` str
  Test.Spec.it "fails for a wrong string" do
    let hello = Data.String.NonEmpty.nes $ SProxy :: SProxy "hello"
    x' <- run' " " (Run.Parser.Combinator.string hello)
    case x' of
      Left x -> do
        x.expected `Test.Spec.Assertions.shouldContain` Run.Parser.chunk hello
        x.unexpected `Test.Spec.Assertions.shouldEqual` Run.Parser.chunk (Data.String.NonEmpty.singleton $ point ' ')
      Right x -> Test.Spec.Assertions.fail ("Should not have succeeded, but we received: " <> show x)

token ::
  Spec Unit
token = Test.Spec.describe "token" do
  Test.Spec.it "fails for empty input" do
    x' <- run' "" (Run.Parser.Combinator.token (const $ pure unit) Nothing)
    case x' of
      Left x -> x.unexpected `Test.Spec.Assertions.shouldEqual` Run.Parser.endOfInput
      Right x -> Test.Spec.Assertions.fail ("Should not have succeeded, but we received: " <> show x)
  Test.Spec.it "returns the value for a 'pure' parser" $ run " " do
    x <- Run.Parser.Combinator.token (const $ pure unit) Nothing
    x `shouldEqual` unit
  Test.Spec.it "consumes input appropriately" $ run "hello" do
    let char c code
          | point c == code = pure c
          | otherwise = Left { expected: Data.Set.singleton (Run.Parser.chunk $ Data.String.NonEmpty.singleton $ point c)
                             , unexpected: Run.Parser.chunk (Data.String.NonEmpty.singleton code)
                             }
    h <- Run.Parser.Combinator.token (char 'h') Nothing
    h `shouldEqual` 'h'
    e <- Run.Parser.Combinator.token (char 'e') Nothing
    e `shouldEqual` 'e'
    l <- Run.Parser.Combinator.token (char 'l') Nothing
    l `shouldEqual` 'l'
    l' <- Run.Parser.Combinator.token (char 'l') Nothing
    l' `shouldEqual` 'l'
    o <- Run.Parser.Combinator.token (char 'o') Nothing
    o `shouldEqual` 'o'
    state <- Run.Parser.get
    Run.liftEffect (Test.Spec.Console.write "checking column")
    state.position.column `shouldEqual` Data.Natural.intToNat 5
    Run.liftEffect (Test.Spec.Console.write "checking line")
    state.position.line `shouldEqual` zero
    state.rest `shouldEqual` ""

try ::
  Spec Unit
try = Test.Spec.describe "try" do
  Test.Spec.it "behaves like the parser when successful" $ quickCheck genUnicodeNonEmptyString \str ->
    do
      let input = Data.String.NonEmpty.toString str
          original = ado
            parse <- parser
            state <- Run.Parser.get
            in { parse
               , state
               }
          parser = Run.Parser.Combinator.string str
          tried = ado
            parse <- Run.Parser.Combinator.try parser
            state <- Run.Parser.get
            in { parse
               , state
               }
      originalParse <- run' input original
      triedParse <- run' input tried
      triedParse `Test.Spec.Assertions.shouldEqual` originalParse
  Test.Spec.it "backtracks when not successful" do
    let hello = Data.String.NonEmpty.nes $ SProxy :: SProxy "hello"
    x' <- run' " " (Run.Parser.Combinator.try $ Run.Parser.Combinator.string hello)
    case x' of
      Left x -> do
        x.expected `Test.Spec.Assertions.shouldContain` Run.Parser.chunk hello
        x.position.column `Test.Spec.Assertions.shouldEqual` zero
        x.position.line `Test.Spec.Assertions.shouldEqual` zero
        x.unexpected `Test.Spec.Assertions.shouldEqual` Run.Parser.chunk (Data.String.NonEmpty.singleton $ point ' ')
      Right x -> Test.Spec.Assertions.fail ("Should not have succeeded, but we received: " <> show x)

-- Boilerplate
type EFFECT r
  = (effect :: Run.EFFECT | r)

type AFF r
  = (aff :: Run.AFF | r)

point ::
  Char ->
  CodePoint
point = Data.String.codePointFromChar

codePointGen ::
  Gen CodePoint
codePointGen = map point Data.Char.Gen.genUnicodeChar

file ::
  AnySandboxedFile
file = Right (Pathy.sandboxAny $ Pathy.file $ SProxy :: SProxy "test/Test/Main.purs")

genUnicodeNonEmptyString ::
  forall f.
  MonadGen f =>
  MonadRec f =>
  f NonEmptyString
genUnicodeNonEmptyString = do
  head <- map Data.String.codePointFromChar Data.Char.Gen.genUnicodeChar
  tail <- Data.String.Gen.genUnicodeString
  pure (Data.String.NonEmpty.cons head tail)

quickCheck ::
  forall a.
  Gen a ->
  (a -> Aff Unit) ->
  Aff Unit
quickCheck x f = do
  xs <- Effect.Class.liftEffect (Test.QuickCheck.Gen.randomSample' 100 x)
  for_ xs f

run ::
  String ->
  Run (AFF + EFFECT + PARSER + ()) Unit ->
  Aff Unit
run str x' = Run.runBaseAff' (Run.Except.catch throw $ Run.Parser.run file str x')
  where
  throw ::
    Error ->
    Run (AFF + EFFECT + ()) Unit
  throw x = Run.liftAff (Test.Spec.Assertions.fail $ "Assertion failed: " <> prettyError x)

run' ::
  forall a.
  String ->
  Run (PARSER + AFF + ()) a ->
  Aff (Either Error a)
run' str x = Run.runBaseAff (Run.Except.runExcept $ Run.Parser.run file str x)

shouldEqual ::
  forall a r.
  Eq a =>
  Show a =>
  a ->
  a ->
  Run (AFF + r) Unit
shouldEqual x y = Run.liftAff (Test.Spec.Assertions.shouldEqual x y)
